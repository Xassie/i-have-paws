## @package whytest
# @author Xassie
# @date 18 Dec 2019
# @brief Why(Py)tests

import meow

def test_decode_wellcoded():
    """! Проверка при полностью верном вводе"""
    assert meow.decode('_..._..___.____.._') == 'MACWOU'
    assert meow.decode('.______.._..._.') == 'COMMA'


def test_decode_wellsymbolsbutno():
    """! Проверка при кривой последовательности"""
    assert not meow.decode('___._..._._._.')


def test_decode_extra_symbols():
    """! Проверка при посторонних символах"""
    assert not meow.decode('I like trains')


def test_decode_singular_sym():
    """! Проверка при одиночном символе"""
    assert not meow.decode('_')


def test_decode_empty_string():
    """! Проверка подачи пустой строки"""
    assert not meow.decode('')


def test_mcheck_correct_msg():
    """! Проверка при полностью верном вводе"""
    assert meow.mcheck('.___._.___.')


def test_mcheck_one_corr_sym():
    """! Проверка при однотипных(ом) символах(е)"""
    assert meow.mcheck('_')
    assert meow.mcheck('.')


def test_mcheck_one_inc_sym():
    """! Проверка при одиночном постороннем вводе"""
    assert not meow.mcheck('F')
    assert not meow.mcheck('-')


def test_mcheck_weird_msg():
    """! Проверка с посторонними символами"""
    assert not meow.mcheck('I like trains')
