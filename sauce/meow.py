## @package meow
# @author Xassie
# @date 18 Dec 2019
# @brief Позовите на помощь.


import argparse


## Кодовый алфавит в виде словаря-константы.
ALPHABET = {
    '___': 'O',
    '__.': 'E',
    '_._': 'W',
    '_..': 'M',
    '.__': 'C',
    '._.': 'A',
    '.._': 'U',
    '...': 'Q',

}

def parse():
    """! Принимает аргумент через командную строку.

    Принимает драгоценное сообщение пользователя, которое его 
    величество желает декодировать. Сама по себе функция ничего
    не принимает.

    Args:
        None
    
    Returns:
        R.n (str): Ввод пользователя при запуске через командную строку.
    
    Raises:
        None


    Examples:
        >>> python meow.py 'something'
        'something'
        >>> python meow.py '____.___.__.'
        '____.___.__.'
    
    """

    parser = argparse.ArgumentParser(description="Give a line to decode")
    parser.add_argument('n', type=str, help='Coded message')

    R = parser.parse_args()
    return R.n

def decode(message):
    """! Декодирует сообщение на основе известного кодового алфавита.

    Принимает закодированное сообщение и декодирует его на
    основе заранее известных соответствий алфавитов.
    
    Args:
        message (str):    Сообщение для декодирования.
        dc (str):     Поочереди собирает элементы message.
        res (str):    Собирает декодированные символы из dc.

    Returns:
        res: Результат расшифровки. Если невозможно декодировать, то None.

    Raises:
        TypeError


    Examples:
        >>> decode('____.____')
        'OWO'
        >>> decode('___..')
        None
        >>> decode('SAUCE')
        None
        >>> decode(2020)
        Traceback (most recent call last):
            ...
        TypeError: 'int' object is not iterable
    """
    
    dc = ''
    res = ''
    for i in message:
        dc += i
        if dc in ALPHABET:
            res += ALPHABET[dc]
            dc = ''
    if not dc:
        return res


def mcheck(message):
    """! Проверка на отсутствие посторонних символов.

    Проверяет есть ли в поданном сообщении символы,
    отсутствущие в кодовом алфавите. Принимает сообщение.
    Если все символы верны, то возвращает 1. В противном
    случае None.

    Args (str):
        message: Пользовательское сообщение.

    Returns:
        Около-булевое значение как результат проверки.

    Raises:
        TypeError


    Examples:
        >>> mcheck('.__..__..')
        1
        >>> mcheck('_')
        1
        >>> mcheck('Friendship is magic!')
        None
        >>> mcheck(2020)
        Traceback (most recent call last):
            ...
        TypeError: 'int' object is not iterable
    """
    
    if set(message).union(set(['_', '.'])) == {'.', '_'}:
        return 1


def main():
    """! Основная логика программы.

    Приветствует Мир-тян. Регулирует последовательность выполнения функций. 
    Делает всю грязную работу. Выводит пользователю в консоль результат.

    Args:
        msg (str):    Зашифрованное сообщение.
        result (str): Результат расшифровки

    Returns:
        Сама по себе функция ничего не возвращает.

    Raises:
        None


    Examples:
        >>> main()
        None
    """

    print('Hello world-chan!')
    msg = parse()
    if mcheck(msg):
        result = decode(msg)
        if result:
            print(result)
        else:
            print('Impossible to decode')
    else:
        print('Please enter valid message')


if __name__ == '__main__':
    main()
